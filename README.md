# krypter

[Live page](https://alamminsalo.gitlab.io/krypter/)

Rust webassembly example project which fetches cryptocurrency price data from coincap.io API and shows them in a UIKit grid, with ability to show historical charts.

Made with [yew framework](https://yew.rs/docs/).
