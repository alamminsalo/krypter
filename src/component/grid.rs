use super::tile::Tile;
use crate::model::{Filter, Message, Sort, State, Ticker, TickerJSON};
use failure::Error;
use serde_derive::Deserialize;
use std::collections::HashMap;
use yew::{
    format::json::Json,
    format::nothing::Nothing,
    html,
    services::console::ConsoleService,
    services::fetch::{FetchService, FetchTask, Request, Response},
    services::websocket::{WebSocketService, WebSocketStatus, WebSocketTask},
    Callback, Component, ComponentLink, Html, Properties, ShouldRender,
};

#[derive(Deserialize)]
struct TickerData {
    data: Vec<TickerJSON>,
}

pub struct Grid {
    state: State,
    filter: Filter,
    data: HashMap<String, Ticker>,
    fetch: FetchService,
    link: ComponentLink<Self>,
    console: ConsoleService,
    fetch_task: Option<FetchTask>,
    ws_task: Option<WebSocketTask>,
    on_tile_clicked: Option<Callback<Ticker>>,
    on_state_update: Option<Callback<State>>,
}

pub enum Msg {
    Refresh,
    Loaded(Vec<Ticker>),
    Error(String),
    TileClicked(Ticker),
    WebSocketReceive(Result<Message, Error>),
    WebSocketStatus(WebSocketStatus),
}

impl Grid {
    fn refresh(&mut self) -> FetchTask {
        let get_assets = Request::get("https://api.coincap.io/v2/assets?limit=100")
            .header("Accept-Encoding", "gzip")
            .body(Nothing)
            .expect("Failed to make request");

        let callback =
            self.link
                .send_back(move |response: Response<Json<Result<TickerData, Error>>>| {
                    let (meta, Json(data)) = response.into_parts();
                    if meta.status.is_success() {
                        let data = data.expect("Failed to marshal response");
                        Msg::Loaded(data.data.into_iter().map(|t| t.into()).collect())
                    } else {
                        Msg::Error(
                            meta.status
                                .canonical_reason()
                                .unwrap_or("Other error")
                                .into(),
                        )
                    }
                });
        self.fetch.fetch(get_assets, callback)
    }
}

#[derive(Properties, PartialEq, Clone)]
pub struct Prop {
    pub on_tile_clicked: Option<Callback<Ticker>>,
    pub on_state_update: Option<Callback<State>>,
    pub state: State,
    pub filter: Filter,
}

impl Default for Prop {
    fn default() -> Self {
        Self {
            state: State::default(),
            filter: Filter::default(),
            on_tile_clicked: None,
            on_state_update: None,
        }
    }
}

impl Component for Grid {
    type Message = Msg;
    type Properties = Prop;

    fn create(prop: Self::Properties, mut link: ComponentLink<Self>) -> Self {
        link.send_self(Msg::Refresh);

        Grid {
            data: HashMap::new(),
            state: prop.state,
            filter: prop.filter,
            link: link,
            fetch: FetchService::new(),
            console: ConsoleService::new(),
            fetch_task: None,
            ws_task: None,
            on_tile_clicked: prop.on_tile_clicked,
            on_state_update: prop.on_state_update,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Refresh => {
                if self.fetch_task.is_none() {
                    self.fetch_task = Some(self.refresh());
                }
                false
            }
            Msg::WebSocketReceive(response) => {
                // iter & update tickers
                let msg = response.unwrap_or(Message::new());
                msg.into_iter().for_each(|(key, val)| {
                    self.data.get_mut(&key).map(|t| {
                        let price: f32 = val.parse().unwrap_or(t.price_usd);
                        t.update_price(price);
                    });
                });
                true
            }
            Msg::WebSocketStatus(_) => false,
            Msg::Loaded(data) => {
                self.data = data.into_iter().map(|t| (t.id.clone(), t)).collect();
                self.fetch_task = None;

                // setup websocket listener for loaded tickers
                let ids: Vec<String> = self.data.keys().map(|id| id.clone()).collect();
                self.ws_task = Some(
                    WebSocketService::new()
                        .connect(
                            &format!("wss://ws.coincap.io/prices?assets={}", &ids.join(",")),
                            self.link
                                .send_back(|Json(data)| Msg::WebSocketReceive(data)),
                            self.link.send_back(|status| Msg::WebSocketStatus(status)),
                        )
                        .unwrap(),
                );

                true
            }
            Msg::Error(msg) => {
                self.console.error(&msg);
                false
            }
            Msg::TileClicked(ticker) => {
                if let Some(ref mut callback) = self.on_tile_clicked {
                    callback.emit(ticker);
                }
                false
            }
        }
    }

    fn change(&mut self, prop: Self::Properties) -> ShouldRender {
        let changed = self.state != prop.state || self.filter != prop.filter;
        self.state = prop.state;
        self.filter = prop.filter;
        self.on_tile_clicked = prop.on_tile_clicked;
        self.on_state_update = prop.on_state_update;

        changed
    }

    fn view(&self) -> Html<Self> {
        // filter by input
        let mut data: Vec<&Ticker> = self
            .data
            .iter()
            .map(|(_, ticker)| ticker)
            .filter(|t| {
                // if input filter is empty
                self.filter.input.len() == 0
                // or if ticker matches input
                    || [&t.symbol.to_lowercase(), &t.name.to_lowercase()]
                        .iter()
                        .any(|s| s.contains(&self.filter.input.to_lowercase()))
            })
            .collect();

        // order by filter sort
        match self.filter.sort {
            Sort::Disabled => data.sort_by_key(|a| a.rank),
            _ => {
                data.sort_by(|a, b| {
                    a.change_percent_24_hr
                        .partial_cmp(&b.change_percent_24_hr)
                        .unwrap()
                });
            }
        }
        if self.filter.sort == Sort::Descending {
            data.reverse();
        }

        // partition pinned & unpinned
        let pinned = data
            .iter()
            .filter(|x| self.state.is_pinned(x))
            .map(|t| (t, true));
        let unpinned = data
            .iter()
            .filter(|x| !self.state.is_pinned(x))
            .map(|t| (t, false));
        let iter = pinned.chain(unpinned);

        html! {
            <div uk-grid="" class="uk-grid-collapse uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-5@m uk-child-width-1-6@l">
            { for iter.map(|(t, pinned)| view_ticker(t, pinned)) }
            </div>
        }
    }
}

fn view_ticker(t: &Ticker, is_pinned: bool) -> Html<Grid> {
    html! {
        <Tile: data=t is_pinned=is_pinned on_clicked=|t| Msg::TileClicked(t) />
    }
}
