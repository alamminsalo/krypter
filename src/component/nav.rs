use crate::model::{Filter, Sort};
use yew::{html, Callback, Component, ComponentLink, Html, Properties, ShouldRender};

pub enum Msg {
    Input(String),
    Sort,
}

pub struct Nav {
    pub filter: Filter,
    pub on_change: Option<Callback<Filter>>,
}

#[derive(Properties, PartialEq, Clone)]
pub struct Prop {
    pub on_change: Option<Callback<Filter>>,
}

impl Default for Prop {
    fn default() -> Self {
        Self { on_change: None }
    }
}

impl Component for Nav {
    type Message = Msg;
    type Properties = Prop;

    fn create(prop: Self::Properties, _: ComponentLink<Self>) -> Self {
        Nav {
            filter: Filter::default(),
            on_change: prop.on_change,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Input(input) => {
                self.filter.input = input;
            }
            Msg::Sort => {
                self.filter.sort = match self.filter.sort {
                    Sort::Disabled => Sort::Descending,
                    Sort::Descending => Sort::Ascending,
                    Sort::Ascending => Sort::Disabled,
                };
            }
        }
        if let Some(ref mut callback) = self.on_change {
            callback.emit(self.filter.clone());
        }
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html<Self> {
        // highlight / alter text based on sorting order
        let baseclass = "nav-btn uk-button uk-button-small";
        let (icon, class, text) = match &self.filter.sort {
            Sort::Disabled => (
                "icon: arrow-down; ratio: 1.3",
                format!("{}", baseclass),
                "Sort",
            ),
            Sort::Descending => (
                "icon: arrow-down; ratio: 1.3",
                format!("{} uk-text-emphasis", baseclass),
                "Desc",
            ),
            Sort::Ascending => (
                "icon: arrow-up; ratio: 1.3",
                format!("{} uk-text-emphasis", baseclass),
                "Asc",
            ),
        };

        html! {
            <div uk-sticky="sel-target: .uk-navbar-container; show-on-up: true; animation: uk-animation-slide-top">
                <nav class="tm-navbar uk-navbar-container uk-light uk-flex" uk-navbar="">
                    // logo
                    <div class="nav-toggle uk-navbar-item">
                        <img uk-img="" data-src="logo.png" height="32" width="140" />
                    </div>
                    // search input
                    <div class="uk-navbar-item uk-width-expand">
                        <form class="nav-toggle uk-search uk-search-navbar uk-width-1-1" hidden="">
                            <input class="uk-search-input" type="search" placeholder="Search..."
                                oninput=|ev| Msg::Input(ev.value) value={&self.filter.input}
                                autofocus="" />
                        </form>
                    </div>
                    // sort btn
                    <div class="nav-toggle uk-navbar-item" hidden="">
                        <button type="button" class={&class} uk-icon={&icon} onclick=|_| Msg::Sort>
                            {text}
                        </button>
                    </div>
                    // search & filter btn
                    <div class="uk-navbar-item">
                        // toggle btn
                        <a uk-search-icon="" class="nav-toggle uk-navbar-toggle uk-text-emphasis"
                        uk-toggle="target: .nav-toggle; animation: uk-animation-fade"/>
                        // close btn
                        <a uk-close="" class="nav-toggle uk-text-emphasis uk-navbar-toggle" hidden=""
                        uk-toggle="target: .nav-toggle; animation: uk-animation-fade"/>
                    </div>
                </nav>
            </div>
        }
    }
}
