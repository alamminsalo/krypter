use super::{Grid, Modal, Nav};
use crate::model::{Currency, Filter, State, Ticker};
use failure::Error;
use serde_derive::Deserialize;
use yew::services::storage::{Area, StorageService};
use yew::{
    format::json::Json,
    format::nothing::Nothing,
    html,
    services::fetch::{FetchService, FetchTask, Request, Response},
    Component, ComponentLink, Html, ShouldRender,
};

const STORE_KEY: &'static str = "state.v1";

#[derive(Deserialize)]
struct CurrencyRates {
    data: Vec<Currency>,
}

pub struct Root {
    modal_ticker: Option<Ticker>,
    state: State,
    storage: StorageService,
    filter: Filter,
    link: ComponentLink<Self>,
    fetch: FetchService,
    currency_fetch: Option<FetchTask>,
}

pub enum Msg {
    FilterUpdate(Filter),
    StateUpdate(State),
    OpenModal(Ticker),
    CloseModal,
    Loaded(Vec<Currency>),
    Error(String),
}

impl Component for Root {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let storage = StorageService::new(Area::Local);
        let mut root = Root {
            modal_ticker: None,
            state: storage.restore(STORE_KEY),
            storage,
            filter: Filter::default(),
            currency_fetch: None,
            fetch: FetchService::new(),
            link,
        };

        root.fetch_data();

        root
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Loaded(rates) => {
                self.currency_fetch = None;
                self.link.send_self(Msg::StateUpdate(State {
                    // only take fiat rates
                    rates: rates
                        .into_iter()
                        .filter(|r| &r.currency_type == "fiat")
                        .filter(|r| &r.id == "euro")
                        .collect(),
                    ..self.state.clone()
                }));
                true
            }
            Msg::Error(_) => {
                self.currency_fetch = None;
                true
            }
            Msg::FilterUpdate(filter) => {
                self.filter = filter;
                true
            }
            Msg::StateUpdate(state) => {
                self.storage.store(STORE_KEY, state);
                self.state = self.storage.restore(STORE_KEY);
                true
            }
            Msg::OpenModal(ticker) => {
                if self.modal_ticker.is_none() {
                    self.modal_ticker = Some(ticker);
                }
                true
            }
            Msg::CloseModal => {
                self.modal_ticker = None;
                true
            }
        }
    }

    fn view(&self) -> Html<Self> {
        html! {
            <div>
                <Nav: on_change=|filter| Msg::FilterUpdate(filter)/>
                <Grid: on_tile_clicked=|ticker| Msg::OpenModal(ticker) state=&self.state.clone()
                    on_state_update=|state| Msg::StateUpdate(state) filter=&self.filter.clone()/>
                {for self.modal_ticker.iter().map(|t| view_modal(t, &self.state))}
            </div>
        }
    }
}

fn view_modal(ticker: &Ticker, state: &State) -> Html<Root> {
    html! {
        <Modal: data=ticker.clone() state=state.clone() on_close=|_| Msg::CloseModal on_state_update=|state| Msg::StateUpdate(state)/>
    }
}

impl Root {
    fn fetch_data(&mut self) {
        let req = Request::get("https://api.coincap.io/v2/rates")
            .header("Accept-Encoding", "gzip")
            .body(Nothing)
            .expect("Failed to make request");

        let callback =
            self.link
                .send_back(|response: Response<Json<Result<CurrencyRates, Error>>>| {
                    let (meta, Json(data)) = response.into_parts();
                    if meta.status.is_success() {
                        let data = data.unwrap();
                        Msg::Loaded(data.data)
                    } else {
                        Msg::Error(
                            meta.status
                                .canonical_reason()
                                .unwrap_or("Other error")
                                .into(),
                        )
                    }
                });
        self.currency_fetch = Some(self.fetch.fetch(req, callback));
    }
}
