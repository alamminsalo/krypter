use crate::model::Ticker;
use yew::{html, Callback, Component, ComponentLink, Html, Properties, ShouldRender};

pub struct Tile {
    data: Ticker,
    on_clicked: Option<Callback<Ticker>>,
    is_pinned: bool,
}

pub enum Msg {
    Clicked,
}

#[derive(Properties, PartialEq, Clone)]
pub struct Prop {
    pub data: Ticker,
    pub is_pinned: bool,
    pub on_clicked: Option<Callback<Ticker>>,
}

impl Default for Prop {
    fn default() -> Self {
        Self {
            data: Ticker::default(),
            on_clicked: None,
            is_pinned: false,
        }
    }
}

impl Component for Tile {
    type Message = Msg;
    type Properties = Prop;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Tile {
            data: props.data,
            on_clicked: props.on_clicked,
            is_pinned: props.is_pinned,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Clicked => {
                if let Some(ref mut callback) = self.on_clicked {
                    callback.emit(self.data.clone());
                }
                false
            }
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.is_pinned = props.is_pinned;
        self.on_clicked = props.on_clicked;
        let changed = self.data != props.data;
        self.data = props.data;
        changed
    }

    fn view(&self) -> Html<Self> {
        let pinned_class = match self.is_pinned {
            true => "uk-button-secondary highlight-dark",
            false => "uk-button-secondary",
        };

        let change = self.data.change_percent_24_hr;
        let change_class = {
            if change > 0.0 {
                "uk-text-success"
            } else {
                "uk-text-danger"
            }
        };

        html! {
            <a class={pinned_class} type="button" onclick=|_| Msg::Clicked>
                <div class="uk-tile">
                    <div class="uk-flex uk-flex-around uk-flex-middle uk-text-small">
                        <div>{ &self.data.symbol}</div>
                        <div class={change_class}>{ &format!("{:+.2}%", change) }</div>
                    </div>
                </div>
            </a>
        }
    }
}
