/**
 * Util functions
 */
use stdweb::js;

// do not display unused code
#[allow(dead_code)]

// displays UIkit notification
pub fn show_notification(msg: &str, status: &str) {
    js! {
        UIkit.notification({
            message: @{msg},
            status: @{status},
            pos: "bottom-center",
            timeout: 3000
        });
    };
}
