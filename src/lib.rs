#![recursion_limit = "256"]

#[macro_use]
extern crate stdweb;

pub mod component;
mod model;

use component::Root;
use stdweb::web::{document, INonElementParentNode};
use wasm_bindgen::prelude::*;
use yew::App;

// Use `wee_alloc` as the global allocator.
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub fn mount(id: &str) {
    let app = App::<Root>::new();
    js! {
        console.log(@{&id});
    };
    let el = document().get_element_by_id(id).expect("element not found");
    app.mount(el);
}
