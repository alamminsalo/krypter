use serde_derive::Serialize;

#[derive(Serialize)]
pub struct Point {
    pub x: u64, // timestamp
    pub y: f32, // price
}
js_serializable!(Point);
