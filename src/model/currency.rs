use serde_derive::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Currency {
    pub id: String,
    pub symbol: String,
    #[serde(rename = "type")]
    pub currency_type: String,
    pub rate_usd: String,
}
