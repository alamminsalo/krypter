#[derive(Clone, PartialEq)]
pub enum Sort {
    Disabled,
    Ascending,
    Descending,
}

#[derive(Clone, PartialEq)]
pub struct Filter {
    pub input: String,
    pub sort: Sort,
}

impl Default for Filter {
    fn default() -> Self {
        Filter {
            input: "".into(),
            sort: Sort::Disabled,
        }
    }
}
