use serde_derive::{Deserialize, Serialize};
use std::time::Duration;

#[derive(Deserialize, Serialize, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct History {
    pub price_usd: String,
    pub time: u64,
}

#[derive(Deserialize, Serialize, Clone, Copy, PartialEq)]
pub enum Interval {
    Hour,
    Day,
    Week,
    Month,
    Year,
}

const DAY_SECS: u64 = 86_400;

impl Into<Duration> for Interval {
    fn into(self) -> Duration {
        match self {
            Interval::Hour => Duration::from_secs(3600),
            Interval::Day => Duration::from_secs(DAY_SECS),
            Interval::Week => Duration::from_secs(DAY_SECS * 7),
            Interval::Month => Duration::from_secs(DAY_SECS * 30),
            Interval::Year => Duration::from_secs(DAY_SECS * 365),
        }
    }
}
js_serializable!(Interval);
