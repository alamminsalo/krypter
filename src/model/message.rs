use std::collections::HashMap;

// websocket message
pub type Message = HashMap<String, String>;
