use crate::model::{Currency, Interval, Ticker};
use failure::Error;
use serde_derive::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, PartialEq, Clone)]
// struct for serializing/deserializing localstorage data
pub struct State {
    // list of pinned ticker ids
    pub pinned_tickers: Vec<String>,
    // history interval
    pub history_interval: Interval,
    // currency rates
    pub rates: Vec<Currency>,
    pub rate_symbol: Option<String>,
}

impl Default for State {
    fn default() -> Self {
        State {
            pinned_tickers: vec![],
            history_interval: Interval::Day,
            rates: vec![],
            rate_symbol: None,
        }
    }
}

impl Into<Result<String, Error>> for State {
    fn into(self) -> Result<String, Error> {
        serde_json::to_string(&self).map_err(|e| e.into())
    }
}

impl From<Result<String, Error>> for State {
    fn from(v: Result<String, Error>) -> Self {
        serde_json::from_str(&v.unwrap_or("".into())).unwrap_or(State::default())
    }
}

impl State {
    pub fn is_pinned(&self, ticker: &Ticker) -> bool {
        self.pinned_tickers.contains(&ticker.id)
    }

    pub fn toggle_pinned(&mut self, ticker: &Ticker) {
        let pinned = toggle_str(self.pinned_tickers.clone(), &ticker.id);
        self.pinned_tickers = pinned;
    }

    pub fn json(&self) -> String {
        serde_json::to_string(&self).unwrap_or("".into())
    }

    pub fn price_pair(&self, price_usd: f32, symbol_pair: &str) -> f32 {
        if let Some(currency) = self.rates.iter().find(|&c| c.symbol == symbol_pair) {
            let usd_ratio = currency.rate_usd.parse().unwrap_or(1.0);
            price_usd / usd_ratio
        } else {
            price_usd
        }
    }
}

// helper fn to toggle string in list
fn toggle_str(mut v: Vec<String>, s: &str) -> Vec<String> {
    match v.contains(&s.into()) {
        true => {
            v.retain(|x| x != s);
        }
        false => {
            v.push(s.into());
        }
    }
    v
}
