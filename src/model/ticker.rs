use serde_derive::Deserialize;

#[derive(PartialEq, Clone)]
pub struct Ticker {
    pub id: String,
    pub rank: i32,
    pub name: String,
    pub symbol: String,
    pub price_usd: f32,
    pub price_usd_open: f32,
    pub market_cap_usd: f64,
    pub change_percent_24_hr: f32,
    pub supply: f64,
}

impl Ticker {
    // updates ticker price, change percent and market cap
    pub fn update_price(&mut self, price: f32) {
        self.price_usd = price;
        self.change_percent_24_hr =
            (self.price_usd - self.price_usd_open) / self.price_usd_open * 100.0;
        self.market_cap_usd = self.supply * self.price_usd as f64;
    }
}

impl Default for Ticker {
    fn default() -> Self {
        Self {
            id: "".into(),
            rank: 0,
            name: "".into(),
            symbol: "".into(),
            price_usd: 0.0,
            price_usd_open: 0.0,
            market_cap_usd: 0.0,
            change_percent_24_hr: 0.0,
            supply: 0.0,
        }
    }
}

// JSON wrapper
#[derive(Deserialize, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct TickerJSON {
    pub id: String,
    pub rank: String,
    pub name: String,
    pub symbol: String,
    pub price_usd: Option<String>,
    pub market_cap_usd: Option<String>,
    pub change_percent_24_hr: Option<String>,
    pub supply: Option<String>,
}

impl Into<Ticker> for TickerJSON {
    fn into(self) -> Ticker {
        let price_usd = self
            .price_usd
            .unwrap_or("0.0".into())
            .parse()
            .unwrap_or(0.0);
        let change_percent_24_hr = self
            .change_percent_24_hr
            .unwrap_or("0.0".into())
            .parse()
            .unwrap_or(0.0);
        let price_usd_open = price_usd / (change_percent_24_hr / 100.0 + 1.0);
        let supply = self.supply.unwrap_or("0.0".into()).parse().unwrap_or(0.0);
        let market_cap_usd = self
            .market_cap_usd
            .unwrap_or("0.0".into())
            .parse()
            .unwrap_or(0.0);

        Ticker {
            id: self.id,
            rank: self.rank.parse().unwrap_or(0),
            name: self.name,
            symbol: self.symbol,
            price_usd,
            price_usd_open,
            change_percent_24_hr,
            market_cap_usd,
            supply,
        }
    }
}
