import init, {mount} from './pkg/krypter.js'

(async () => {
  await init()
  mount("root")
})()
